package ku.util;
/**
 * A stack that does nothing
 * @author Parinvut Rochanavedya
 * @version 31-03-2015
 */
public class DummyStack implements Stack{
	/**
	 * constructor
	 */
	public DummyStack(int capacity){
		
	}
	/**
	 * return the maximum number of objects the stack can hold.
	 */
	public int capacity() {
		// TODO Auto-generated method stub
		return 0;
	}
	/**
	 * true if the stack is empty, false otherwise.
	 */
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	/**
	 * true if stack is full, false otherwise.
	 */
	public boolean isFull() {
		// TODO Auto-generated method stub
		return false;
	}
	/**
	 * return the item on the top of the stack, without removing it. If the stack is empty, return null.
	 */
	public Object peek() {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * Return the item on the top of the stack, and remove it from the stack. Throws: java.util.EmptyStackException if stack is empty.
	 */
	public Object pop() {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * push a new item onto the top of the stack. The parameter (obj) must not be null.
	 * Throws: InvalidArgumentException if parameter is null.
	 * Throws: IllegalStateException if push is called when stack is full.
	 */
	public void push(Object obj) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * return the number of items in the stack. Returns 0 if the stack is empty.
	 */
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

}
