package Test;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import ku.util.DummyStack;
import ku.util.Stack;
import ku.util.StackFactory;
/**
 * A class for test stack cases
 * @author Parinvut Rochanavedya
 * @version 31-03-2015
 */
public class StackTest {
	private Stack stack;
	/** "Before" method is run before each test. */
	@Before
	public void setUp() {
		StackFactory.setStackType(1);
		stack = StackFactory.makeStack(3);
	}
	@Test
	public void newStackIsEmpty() {
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0, stack.size() );
	}
	@Test( expected=java.util.EmptyStackException.class )
	public void popAnEmptyStack(){
		Assume.assumeTrue( stack.isEmpty() );
		stack.pop();
	}
	/** This test *should* throw an exception if stack is correct. */
	@Test( expected=java.lang.IllegalStateException.class )
	public void pushAFullStack(){
		stack = StackFactory.makeStack(3);
		stack.push("objA");
		stack.push("objB");
		stack.push("objC");
		stack.push("objD");
		//fail("Should have thrown an exception already.");
	}
	
	@Test( expected=IllegalArgumentException.class )
	public void pushANullObjectToStack(){
		stack.push(null);
	}
	@Test
	public void peekAnEmptyStack(){
		Assume.assumeTrue( stack.isEmpty() );
		assertTrue( stack.peek() == null);
	}
	@Test( expected=IllegalArgumentException.class )
	public void pushANullObjectToFullStack(){
		this.setUp();
		stack.push("objA");
		stack.push("objB");
		stack.push("objC");
		stack.push(null);
	}
	@Test
	public void testInAndOut(){
		stack = StackFactory.makeStack(3);
		Object e = new Object();
		stack.push(e);
		assertEquals(e,stack.pop());
	}
	@Test
	public void testOneCapacityStack(){
		stack = StackFactory.makeStack(1);
		assertTrue( stack.isEmpty() );
		assertFalse( stack.isFull() );
		assertEquals( 0, stack.size() );
		stack.push("obj");
		assertFalse( stack.isEmpty() );
		assertTrue( stack.isFull() );
		assertEquals( 1, stack.size() );
		stack.pop();
	}
	
}
